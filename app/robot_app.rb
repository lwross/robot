require_relative 'robot'
require_relative 'table'

class RobotApp
  def initialize
    table = Table.new(5, 5)
    @robot = Robot.new(table)
  end

  def run
    loop do
      command = get_user_command()
      @robot.command_robot(command)
    end
  end

  def get_user_command
    command = gets().chomp()
    return command
  end
end

# Let's trap interrupts and exit the application.
Kernel.trap("INT") { exit(0) }

# Run the application
app = RobotApp.new()
app.run()