class Robot

  attr_reader :x_coordinate, :y_coordinate, :facing, :placed_on_surface, :surface

  # Initialise the Robot ready for placing on the given surface.
  # The given surface must respond to valid_position message
  def initialize(surface)
    @surface = surface
    @placed_on_surface = false
  end

  # Place will place the robot on the surface at the given x, y coordinates facing in the given direction
  # Returns true if successful or false if there is no surface assigned or if and parameter is invalid
  def place (x, y, facing)
    return_value = false

    if valid_facing(facing)
      if @surface.valid_position(x, y)
        @x_coordinate = x
        @y_coordinate = y
        @facing = facing
        @placed_on_surface = true
        return_value = true
      end
    end

    return return_value
  end

  # Move the robot 1 place in facing direction
  def move
    return_value = false

    if @placed_on_surface
      new_x_coordinate = @x_coordinate
      new_y_coordinate = @y_coordinate

      case @facing
        when 'NORTH'
          new_y_coordinate += 1
        when 'WEST'
          new_x_coordinate -= 1
        when 'SOUTH'
          new_y_coordinate -= 1
        when 'EAST'
          new_x_coordinate += 1
      end

      if @surface.valid_position(new_x_coordinate, new_y_coordinate)
        @x_coordinate = new_x_coordinate
        @y_coordinate = new_y_coordinate
        return_value = true
      end
    end

    return return_value
  end

  # Rotate the robot 90 degrees left
  def rotate_left
    if @placed_on_surface
      case @facing
        when 'NORTH'
          @facing = 'WEST'
        when 'WEST'
          @facing = 'SOUTH'
        when 'SOUTH'
          @facing = 'EAST'
        when 'EAST'
          @facing = 'NORTH'
      end
    end
  end

  # Rotate the robot 90 degrees right
  def rotate_right
    if @placed_on_surface
      case @facing
        when 'NORTH'
          @facing = 'EAST'
        when 'WEST'
          @facing = 'NORTH'
        when 'SOUTH'
          @facing = 'WEST'
        when 'EAST'
          @facing = 'SOUTH'
      end
    end
  end

  # output the current position and facing
  def report
    if @placed_on_surface
        puts get_report_string()
    end
  end

  def to_s
    report()
  end

  def command_robot(command)
    return_value = true
    case command
      when 'MOVE'
        move()
      when 'LEFT'
        rotate_left()
      when 'RIGHT'
        rotate_right()
      when 'REPORT'
        report()
      else
        parts = command.scan(/\w+/)
        if parts[0] == 'PLACE'
          begin
            place(Integer(parts[1]), Integer(parts[2]), parts[3])
          rescue ArgumentError
            # Ignore invalid command as per specification
            return_value = false
          end
        else
          return_value = false
        end
    end
    return return_value
  end

  private

  # valid_facing is used internally to validate a facing value
  # returns true if valid, false if invalid
  def valid_facing(val)
    valid_facing_values = %w(NORTH SOUTH EAST WEST)
    
    if valid_facing_values.include? val
      return true
    else
      return false
    end
  end

  # Get the string that will be used in the report method
  def get_report_string
    if @placed_on_surface
      return "#{@x_coordinate},#{@y_coordinate},#{@facing}"
    end
  end

  # Remove the robot from the current surface
  def remove_from_surface
    @x_coordinate = nil
    @y_coordinate = nil
    @facing = nil
    @placed_on_surface = false
  end

end