class Table

  attr_reader :width, :height

  def initialize(width, height)
    raise unless width.is_a?(Numeric)
    raise unless height.is_a?(Numeric)
    @width = width
    @height = height
  end

  def valid_position(x, y)
    if x < @width && x >= 0 && y < @height && y >= 0
      return true
    else
      return false
    end
  end

  def to_s
    "Table #{@width} X #{@height}"
  end

end