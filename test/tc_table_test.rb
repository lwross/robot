require 'test/unit'
require_relative '../app/table'

class TableTest < Test::Unit::TestCase

  # set up a Table instance for us to use throughout the test
  def setup
    @table = Table.new(5, 5)
  end

  # Test the type checking of constructor parameters of the Table class
  def test_typecheck
    assert_raise( RuntimeError ) { Table.new(5, 'a') }
    assert_raise( RuntimeError ) { Table.new('a', 5) }
    assert_nothing_raised( RuntimeError ) { Table.new(5, 5) }
  end

  # Test the valid_position method of the Table class
  def test_valid_position
    # Test the valid corners
    assert(@table.valid_position(0, 0))
    assert(@table.valid_position(4, 4))
    assert(@table.valid_position(0, 4))
    assert(@table.valid_position(4, 0))

    # Test some other valid edge cases
    assert(@table.valid_position(2, 0))
    assert(@table.valid_position(2, 4))
    assert(@table.valid_position(0, 2))
    assert(@table.valid_position(4, 2))
  end

  # Test the valid_position method of the Table class
  def test_invalid_position
    # Test some invalid edge cases
    assert(!@table.valid_position(-1, 2))
    assert(!@table.valid_position(5, 2))
    assert(!@table.valid_position(2, -1))
    assert(!@table.valid_position(2, 5))
  end

  def test_to_s
    assert_equal('Table 5 X 5', @table.to_s())
  end

end