require 'test/unit'
require_relative '../app/robot'
require_relative '../app/table'

class RobotTest < Test::Unit::TestCase

  # set up a Robot instance for us to use throughout the test
  def setup
    table = Table.new(5, 5)
    @robot = Robot.new(table)
  end

  # test the Table assignment which occurs in setup
  def test_table_assignment
    assert_kind_of(Table, @robot.surface)
    assert_equal(5, @robot.surface.width)
    assert_equal(5, @robot.surface.height)
  end

  # test the Robot place method
  def test_place
    # test some valid edge cases
    assert(@robot.place(0, 0, 'NORTH'))
    assert(@robot.place(4, 4, 'NORTH'))

    # test some invalid edge cases
    assert(!@robot.place(-1, -1, 'NORTH'))
    assert(!@robot.place(5, 5, 'NORTH'))
    assert(!@robot.place(0, 5, 'NORTH'))
    assert(!@robot.place(5, 0, 'NORTH'))
    assert(!@robot.place(-1, 0, 'NORTH'))
    assert(!@robot.place(0, -1, 'NORTH'))

    # test valid facing cases
    assert(@robot.place(2, 2, 'NORTH'))
    assert(@robot.place(2, 2, 'SOUTH'))
    assert(@robot.place(2, 2, 'EAST'))
    assert(@robot.place(2, 2, 'WEST'))

    # test invalid facing case
    assert(!@robot.place(2, 2, 'INVALID_FACING'))
  end

  def test_valid_facing
    assert(@robot.send(:valid_facing, 'NORTH'))
    assert(@robot.send(:valid_facing, 'SOUTH'))
    assert(@robot.send(:valid_facing, 'EAST'))
    assert(@robot.send(:valid_facing, 'WEST'))
    assert(!@robot.send(:valid_facing, 'INVALID_FACING'))
  end

  def test_rotate_left_before_place
    # test calling rotate_left method BEFORE a place command has been issued
    @robot.send(:remove_from_surface)
    @robot.rotate_left()
    assert_nil(@robot.facing)
  end

  def test_rotate_left_after_place
    # test placing robot on surface facing north then rotating left 4 times
    @robot.place(2,2,'NORTH')
    assert_equal('NORTH', @robot.facing)
    @robot.rotate_left()
    assert_equal('WEST', @robot.facing)
    @robot.rotate_left()
    assert_equal('SOUTH', @robot.facing)
    @robot.rotate_left()
    assert_equal('EAST', @robot.facing)
    @robot.rotate_left()
    assert_equal('NORTH', @robot.facing)
  end

  def test_rotate_right
    # test calling rotate_right method BEFORE a place command has been issued
    @robot.send(:remove_from_surface)
    @robot.rotate_right()
    assert_nil(@robot.facing)
  end

  def test_rotate_right_after_place
    # test placing robot on surface facing north then rotating right 4 times
    @robot.place(2,2,'NORTH')
    assert_equal('NORTH', @robot.facing)
    @robot.rotate_right()
    assert_equal('EAST', @robot.facing)
    @robot.rotate_right()
    assert_equal('SOUTH', @robot.facing)
    @robot.rotate_right()
    assert_equal('WEST', @robot.facing)
    @robot.rotate_right()
    assert_equal('NORTH', @robot.facing)
  end

  # test calling rotate_right method BEFORE a place command has been issued
  def test_move_before_place
    @robot.send(:remove_from_surface)
    assert(!@robot.move())
    assert_nil(@robot.facing)
    assert_nil(@robot.x_coordinate)
    assert_nil(@robot.y_coordinate)
  end

  # test placing on edge then falling off each edge (must not fall off!)
  def test_move_off_edge
    @robot.place(0, 2, 'WEST')
    assert_equal('WEST', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(!@robot.move())
    assert_equal('WEST', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 0, 'SOUTH')
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)
    assert(!@robot.move())
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)

    @robot.place(4, 2, 'EAST')
    assert_equal('EAST', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(!@robot.move())
    assert_equal('EAST', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 4, 'NORTH')
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
    assert(!@robot.move())
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
  end

  # test placing next to edge then moving to very edge (valid move)
  def test_move_to_edge
    @robot.place(1, 2, 'WEST')
    assert_equal('WEST', @robot.facing)
    assert_equal(1, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('WEST', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 1, 'SOUTH')
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(1, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)

    @robot.place(3, 2, 'EAST')
    assert_equal('EAST', @robot.facing)
    assert_equal(3, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('EAST', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 3, 'NORTH')
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(3, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
  end

  # test placing on edge then moving away from edge (valid move)
  def test_move_from_edge
    @robot.place(0, 2, 'EAST')
    assert_equal('EAST', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('EAST', @robot.facing)
    assert_equal(1, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 0, 'NORTH')
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('NORTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(1, @robot.y_coordinate)

    @robot.place(4, 2, 'WEST')
    assert_equal('WEST', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('WEST', @robot.facing)
    assert_equal(3, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)

    @robot.place(2, 4, 'SOUTH')
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('SOUTH', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(3, @robot.y_coordinate)
  end

  def test_move_along_edge
    # test placing on edge then moving along the edge (valid move)
    @robot.place(0, 2, 'SOUTH')
    assert_equal('SOUTH', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('SOUTH', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(1, @robot.y_coordinate)

    @robot.place(0, 2, 'NORTH')
    assert_equal('NORTH', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('NORTH', @robot.facing)
    assert_equal(0, @robot.x_coordinate)
    assert_equal(3, @robot.y_coordinate)

    @robot.place(2, 0, 'EAST')
    assert_equal('EAST', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('EAST', @robot.facing)
    assert_equal(3, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)

    @robot.place(2, 0, 'WEST')
    assert_equal('WEST', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('WEST', @robot.facing)
    assert_equal(1, @robot.x_coordinate)
    assert_equal(0, @robot.y_coordinate)

    @robot.place(4, 2, 'SOUTH')
    assert_equal('SOUTH', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('SOUTH', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(1, @robot.y_coordinate)

    @robot.place(4, 2, 'NORTH')
    assert_equal('NORTH', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(2, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('NORTH', @robot.facing)
    assert_equal(4, @robot.x_coordinate)
    assert_equal(3, @robot.y_coordinate)

    @robot.place(2, 4, 'EAST')
    assert_equal('EAST', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('EAST', @robot.facing)
    assert_equal(3, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)

    @robot.place(2, 4, 'WEST')
    assert_equal('WEST', @robot.facing)
    assert_equal(2, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
    assert(@robot.move())
    assert_equal('WEST', @robot.facing)
    assert_equal(1, @robot.x_coordinate)
    assert_equal(4, @robot.y_coordinate)
  end

  def test_get_report_string
    @robot.place(2,2,'NORTH')
    assert_equal('2,2,NORTH', @robot.send(:get_report_string))
  end

  def test_command_robot
    # valid commands
    assert(@robot.command_robot('PLACE 0,0,NORTH'))
    assert(@robot.command_robot('MOVE'))
    assert(@robot.command_robot('LEFT'))
    assert(@robot.command_robot('RIGHT'))
    assert(@robot.command_robot('REPORT'))
    assert_equal('0,1,NORTH', @robot.send(:get_report_string))

    # invalid commands
    assert(!@robot.command_robot('PLACE INVALID_ARG,0,NORTH'))
    assert(!@robot.command_robot('JUMP'))
  end

end