# Toy Robot Simulator

This application is a simulation of a toy robot moving on the surface of a square 5 X 5 unit table

The application runs on the command line and can follow the following commands:

* PLACE X,Y,F
* MOVE
* LEFT
* RIGHT
* REPORT

The robot adheres to Isaac Asimov's "Three Laws of Robotics", in particular rule 3. A robot must protect its own existence as long as such protection does not conflict with the First or Second Law.  For this reason, the robot will ignore any command which would cause it to fall off the table.

##Usage:

    $ ruby app/robot_app.rb

Then enter commands.  For example:

a)

    PLACE 0,0,NORTH
    MOVE
    REPORT

    Output: 0,1,NORTH

b)

    PLACE 0,0,NORTH
    LEFT
    REPORT

    Output: 0,0,WEST

c)

    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT

    Output: 3,3,NORTH

**To exit the application, interrupt with CTRL-C**

##Testing:

    $ ruby test/ts_robot_app_components_test.rb